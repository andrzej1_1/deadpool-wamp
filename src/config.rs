use std::convert::Infallible;

use crate::{CreatePoolError, Manager, Pool, PoolBuilder, PoolConfig, Runtime};

/// Configuration object.
///
/// # Example (from environment)
///
/// By enabling the `serde` feature you can read the configuration using the
/// [`config`](https://crates.io/crates/config) crate as following:
/// ```env
/// WAMP__URL=ws://127.0.0.1:8080/ws
/// WAMP__POOL__MAX_SIZE=16
/// WAMP__POOL__TIMEOUTS__WAIT__SECS=2
/// WAMP__POOL__TIMEOUTS__WAIT__NANOS=0
/// ```
/// ```rust
/// # use serde_1 as serde;
/// #
/// #[derive(serde::Deserialize, serde::Serialize)]
/// # #[serde(crate = "serde_1")]
/// struct Config {
///     sqlite: deadpool_sqlite::Config,
/// }
///
/// impl Config {
///     pub fn from_env() -> Result<Self, config::ConfigError> {
///         let mut cfg = config::Config::new();
///         cfg.merge(config::Environment::new().separator("__")).unwrap();
///         cfg.try_into()
///     }
/// }
/// ```

#[derive(Clone, Debug, Default)]
#[cfg_attr(feature = "serde", derive(serde_1::Deserialize, serde_1::Serialize))]
#[cfg_attr(feature = "serde", serde(crate = "serde_1"))]
pub struct Config {
  /// WAMP server URL.
  pub url: Option<String>,

  /// WAMP realm to join.
  pub realm: Option<String>,

  /// [`Pool`] configuration.
  pub pool: Option<PoolConfig>,
}

impl Config {
  /// Creates a new [`Pool`] using this [`Config`].
  ///
  /// # Errors
  ///
  /// See [`CreatePoolError`] for details.
  pub fn create_pool(&self, runtime: Runtime) -> Result<Pool, CreatePoolError> {
    self
      .builder(runtime)
      .map_err(CreatePoolError::Config)?
      .build()
      .map_err(CreatePoolError::Build)
  }

  /// Creates a new [`PoolBuilder`] using this [`Config`].
  ///
  /// # Errors
  ///
  /// See [`ConfigError`] for details.
  ///
  /// [`RedisError`]: redis::RedisError
  pub fn builder(&self, runtime: Runtime) -> Result<PoolBuilder, ConfigError> {
    let manager = Manager::from_config(self);
    Ok(
      Pool::builder(manager)
        .config(self.get_pool_config())
        .runtime(runtime),
    )
  }

  /// Returns [`deadpool::managed::PoolConfig`] which can be used to construct
  /// a [`deadpool::managed::Pool`] instance.
  #[must_use]
  pub fn get_pool_config(&self) -> PoolConfig {
    self.pool.unwrap_or_default()
  }
}

/// This error is returned if there is something wrong with the WAMP configuration.
///
/// This is just a type alias to [`Infallible`] at the moment as there
/// is no validation happening at the configuration phase.
pub type ConfigError = Infallible;
