#![doc = include_str!("../README.adoc")]
#![deny(
  nonstandard_style,
  rust_2018_idioms,
  rustdoc::broken_intra_doc_links,
  rustdoc::private_intra_doc_links
)]
#![forbid(non_ascii_idents, unsafe_code)]
#![warn(
  deprecated_in_future,
  missing_copy_implementations,
  missing_debug_implementations,
  missing_docs,
  unreachable_pub,
  unused_import_braces,
  unused_labels,
  unused_lifetimes,
  unused_qualifications,
  unused_results
)]

mod config;

use deadpool::{async_trait, managed};

pub use self::config::{Config, ConfigError};

pub use deadpool::managed::reexports::*;
deadpool::managed_reexports!(
  "wamp",
  Manager,
  deadpool::managed::Object<Manager>,
  wamp_async::WampError,
  ConfigError
);

/// Type alias for ['Object'].
pub type Client = managed::Object<Manager>;

/// Type alias for using [`deadpool::managed::RecycleResult`] with [`redis`].
type RecycleResult = managed::RecycleResult<wamp_async::WampError>;

/// [`Manager`] for creating and recycling [`wamp_async::Client`].
///
/// [`Manager`]: managed::Manager
#[derive(Debug)]
pub struct Manager {
  config: Config,
}

impl Manager {
  /// Creates a new [`Manager`] using the given [`Config`].
  #[must_use]
  pub fn from_config(config: &Config) -> Self {
    Self {
      config: config.clone(),
    }
  }
}

#[async_trait]
impl managed::Manager for Manager {
  type Type = wamp_async::Client<'static>;
  type Error = wamp_async::WampError;

  async fn create(&self) -> Result<wamp_async::Client<'static>, Self::Error> {
    let url = self
      .config
      .url
      .as_deref()
      .unwrap_or("ws://127.0.0.1:8080/ws");

    let (mut client, (evt_loop, _rpc_evt_queue)) = wamp_async::Client::connect(
      url,
      Some(wamp_async::ClientConfig::default().set_ssl_verify(false)),
    )
    .await?;

    drop(tokio::spawn(evt_loop));

    let wamp_realm = self.config.realm.as_deref().unwrap_or("realm1");
    client.join_realm(wamp_realm).await?;

    Ok(client)
  }

  async fn recycle(&self, client: &mut wamp_async::Client<'static>) -> RecycleResult {
    if !client.is_connected() {
      eprintln!("WAMP connection could not be recycled: Connection closed");
      return Err(managed::RecycleError::StaticMessage("Connection closed"));
    }
    //client.disconnect(); // TODO: Fix call by using wrapper.
    Ok(())
  }
}
